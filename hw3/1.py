names = input("Введите имена персонажей через запятую \n")  # запрашиваем ввод, принимаем его с новой строки
names = names.lower().split(", ")  # переводим строку в нижний регистр, разбиваем по запятой и пробелу, получаем список
names2 = str("".join(names))  # склеиваем список в строку без пробелов
print(len(set(names2)))  # выводим размер множества букв строки
