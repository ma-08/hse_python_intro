from random import randint
import pandas as pd
import pymorphy2
import re


class Game:
    def __init__(self):
        self.words = pd.read_csv("frequency_list.csv", sep="\t")
        self.list_of_words = [x for x in self.words["Lemma"]]
        self.word = ''
        self.your_word = ""
        self.conjectures = 0  # здесь будет храниться количество догадок пользователя
        self.bulls = 0
        self.cows = 0
        self.game_mood_v = 0
        self.storage_of_words = []
        self.morph = pymorphy2.MorphAnalyzer()
        self.game_mood_choosing()

    def is_cyrillic(self, text):  # эта функция для проверяет, состоит ли строка от пользователя только из русских букв
        return bool(re.fullmatch(r'[а-яё]+', text))

    def counting_of_bulls_and_cows(self):
        w = self.your_word
        w1 = self.word
        for i in range(len(self.your_word)):  # подсчет быков
            if self.your_word[i] == self.word[i]:
                self.bulls += 1
                w = w.replace(w[i], "+")
                w1 = w1.replace(w1[i], "+")
        for j in range(len(w)):  # подсчет коров
            if w[j] in w1 and w[j] != "+":
                self.cows += 1
                w1.replace(w[j], "")
        return self.bulls, self.cows

    def game_mood_choosing(self):  # выбираем режим игры
        game_mood = input('Приветствуем Вас в игре "Быки и коровы"! Пожалуйста, выберите режим игры.\n'
                          'Вы хотите играть с повторениями букв в загаданном слове (да/нет)?>> ')
        if game_mood.strip().lower() == "да":
            self.choice_of_word_with_repetitions()
        elif game_mood.strip().lower() == "нет":
            self.game_mood_v = 1
            self.choice_of_word_without_repetitions()
        else:  # если ввод некорректен, запускаем функцию ещё раз
            print("Извините, мы Вас не поняли. Пожалуйста, попробуйте выбрать заново.\n")
            self.game_mood_choosing()

    def choice_of_word_without_repetitions(self):  # вариант без повторений
        len_of_word = input("Введите длину слова для угадывания (целое число)>> ")
        if not len_of_word.isdigit():
            print("То, что Вы ввели, не может быть длиной слова. Пожалуйста, попробуйте ещё раз.")
            self.choice_of_word_without_repetitions()
        len_of_word = int(len_of_word)
        for x in self.words["Lemma"]:
            if len(x) == len_of_word and len(set(x)) == len(x) and '-' not in x and len(set(x.lower())) == len(x):
                self.storage_of_words.append(x)
        if not self.storage_of_words:
            print("Извините, слово такой длины загадать нельзя. Пожалуйста, выберите другое значение.")
            self.choice_of_word_without_repetitions()
        self.word = self.storage_of_words[randint(0, len(self.storage_of_words))]
        self.guessing()

    def choice_of_word_with_repetitions(self):  # вариант с повторениями
        len_of_word = input("Введите длину слова для угадывания (целое число)>> ")
        if not len_of_word.isdigit():
            print("То, что Вы ввели, не может быть длиной слова. Пожалуйста, попробуйте ещё раз.")
            self.choice_of_word_with_repetitions()
        len_of_word = int(len_of_word)
        for x in self.words["Lemma"]:
            if len(x) == len_of_word and len(set(x)) == len(x) and '-' not in x:
                self.storage_of_words.append(x)
        if not self.storage_of_words:
            print("Извините, слово такой длины загадать нельзя. Пожалуйста, выберите другое значение.")
            self.choice_of_word_without_repetitions()
        self.word = self.storage_of_words[randint(0, len(self.storage_of_words))]
        self.guessing()

    def guessing(self):  # процесс угадывания
        bulls_word = self.morph.parse('бык')[0]
        cows_word = self.morph.parse('корова')[0]
        conj_word = self.morph.parse('попытка')[0]
        is_guessed = False
        surrender = False
        while not is_guessed and not surrender:
            self.your_word = input("Введите свою догадку>> ").strip().lower()
            if len(self.your_word) != len(self.word) or not self.your_word.isalpha() \
                    or not self.is_cyrillic(self.your_word) or len(self.your_word) < 1:
                print("Слово не соответствует условию. Попытка не засчитывается, попробуйте ещё раз.")
                continue
            elif self.your_word not in self.list_of_words:  # если слова нет в частотном списке
                ad = input("Нам кажется, что такого слова не существует. Так ли это (да/нет)? ")
                if ad.lower().strip() == "нет":  # и пользователь настаивает, что слово существует
                    ad_ver = input('Интересно! Хотите, чтобы мы его выучили (да/нет)? ')
                    if ad_ver.strip().lower() == "да":  # то, если пользователь согласен
                        self.storage_of_words.append(self.your_word)  # добавляем в наш список слов
                        print("Спасибо! Но эту попытку мы решили не засчитывать. Давайте дальше)")
                        continue
                print("Попытка не засчитана. Попробуйте ещё.")
                continue
            elif self.game_mood_v == 1 and len(set(self.your_word)) < len(self.your_word):  # в режиме без повторений
                print("Вы выбрали режим без повторений букв, а в Вашем слове они повторяются. Попытка не засчитана. "
                      "Попробуйте ещё.")  # не засчитываем за попытку слово с повторениями
                continue
            self.conjectures += 1
            if self.your_word == self.word:
                print("Невероятно, Вы угадали! Поздравляем!")
                print(f"Вам потребовалось {self.conjectures} {conj_word.make_agree_with_number(self.conjectures).word}")
                is_guessed = True
            else:
                b_c = self.counting_of_bulls_and_cows()
                self.bulls = b_c[0]
                self.cows = b_c[1]
                print(f"{self.bulls} {bulls_word.make_agree_with_number(self.bulls).word}, "  # согласуем быков и коров
                      f"{self.cows} {cows_word.make_agree_with_number(self.cows).word}")  # с числительными
                self.bulls = 0
                self.cows = 0
                sur = input("Хотите сдаться (да/нет)?>> ")
                if sur.lower() == "да":
                    surrender = True
                    print(f"А правильный ответ был {self.word} :-)")


def main():
    Game()


if __name__ == "__main__":
    main()
