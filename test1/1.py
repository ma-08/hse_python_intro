name = input("Представьтесь, пожалуйста (без пробелов) ")
data = [int(x) for x in input("Введите Вашу дату рождения в формате ХХ.ХХ.ХХХХ ").split(".")]
numb = data[0] // 10 + data[0] % 10 + data[1] // 10 + data[1] % 10 + data[2] // 1000 + data[2] % 1000 // 100 + \
    data[2] % 100 // 10 + data[2] % 10
c = 0
while numb > 10:
    c += numb % 10
    numb //= 10
c += numb  # теперь здесь число судьбы
f = open("numerology.txt", "r")
text = f.readlines()
d = {}
for i in range(len(text)):
    if "ЗНАЧЕНИЕ ЧИСЛА СУДЬБЫ" in text[i]:
        d[text[i][-2]] = text[i+1]
f.close()
e = open(f"{name}_предсказание.txt", "w")
e.write(f"Доброго времени суток, {name}!\n")
e.write(f"Ваше число судьбы - {c}.\n")
e.write(d[str(c)])
