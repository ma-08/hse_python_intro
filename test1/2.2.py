alph = ["а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с",
        "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я"]
d = {}
for i in range(len(alph)):
    if i + 22 < 33:
        print(i+22)
        d[alph[i]] = alph[i + 22]
    else:
        c = 22 - (32 - i)
        d[alph[i]] = alph[c]
reg = input("Выберите режим работы: Зашифровать или Расшифровать ")
path = input("Введите путь к файлу с сообщением ")
f = open(path, "r+", encoding="UTF-8")
text = f.readlines()
if reg.strip() == "Зашифровать":
    new_text = ""
    for y in text:
        if str(y).lower() in alph:
            new_text += d[y.lower()]
        else:
            new_text += y
    f.write(new_text)
elif reg.strip() == "Расшифровать":
    new_text = ""
    for y in text:
        if str(y).lower() in d.values():
            for x in d.items():
                if d[x] == str(y).lower():
                    new_text += x
        else:
            new_text += y
    f.write("\n")
    f.write(new_text)
f.close()
