data = {}
books = 0
journals = 0


class Item:
    def __init__(self, item_id, title, author):
        self.item_id = item_id
        self.title = title
        self.author = author
        data[self.item_id] = [self.title, self.author]

    def __str__(self):
        return f'The {self.title} of {self.author} has an id {self.item_id}'



class Book(Item):
    def __init__(self, item_id, title, author, number_of_pages):
        self.item_id = item_id
        self.title = title
        self.author = author
        self.number_of_pages = number_of_pages

    def __str__(self):
        return f'The book {self.title} of {self.author} has an id {self.item_id} and {self.number_of_pages} pages'


class Journal(Item):
    def __init__(self, item_id, title, author, issue_number):
        self.item_id = item_id
        self.title = title
        self.author = author
        self.issue_number = issue_number

    def __str__(self):
        return f'The book {self.title} of {self.author} has an id {self.item_id} and {self.issue_number} issue_number'


class Library:
    def __init__(self, name=list, items=list, *a):
        self.name = name
        self.items = items
        self.a = a
        if self.a == "Book":
            books += 1
        elif self.a == "Journal":
            journals += 1
    def get_item(self, item):
        self.items.append(item)

    def find_item(item_id):
        print(Item(item_id, data[item_id][0], data[item_id][1]).__str__())

    def remove_item(item_id):
        data[item_id].pop()

    def count_books(self):
        return books

    def count_journals(self):
        return journals


def from_csv(path):
    t = open(path).readlines()
    for line in t:
        return Library(line[2], line[1], line[0])


print(Book(1, "название", "автор", 127))