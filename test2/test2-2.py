import pandas as pd
df = pd.read_csv("https://raw.githubusercontent.com/vantral/files/main/RickAndMortyScripts.csv")


def count_values(df1, normalize=False):
    print(f'Рик {df1.value_counts(normalize=normalize)["Rick"]}, '
          f'Морти {df1.value_counts(normalize=normalize)["Morty"]}')


def max_episode():
    print(df['episode name'].max())


count_values(df['name'], normalize=True)
max_episode()