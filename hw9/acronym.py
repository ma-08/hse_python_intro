import re
with open(input("Введите название файла "), encoding="UTF-8") as f:
    data = f.read()
# выводим без разделителей в одну строку первый символ в заглавном регистре каждой найденной последовательности
print(*[x[0].upper() for x in re.findall(r"\b[А-Яа-яЁё]+\b", data)], sep="")
