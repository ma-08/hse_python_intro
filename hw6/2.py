import pathlib
p = pathlib.Path('love_and_piece')
f = open("war_and_piece1.txt", 'w')  # создаём файл для записи всего романа
s = [x for x in p.iterdir()]  # создаём список папок-абзацев
for y in s:  # в каждой папке-абзаце
    for z in y.iterdir():
        e = open(z, "r")  # открываем поочерёдно все файлы с предложениями
        f.write(e.read())  # записываем их содержимое в итоговый файл
        e.close()  # закрываем файл с предложением
f.close()  # закрываем итоговый файл
