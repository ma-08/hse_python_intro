import pathlib
p = pathlib.Path('love_and_piece')  # создаём новую директорию
p.mkdir(exist_ok=True)  # передаём в mkdir параметр, чтобы код не падал с ошибкой при повторном запуске
f = open("war_and_piece.txt", 'r')  # открываем файл в режиме чтения
p = pathlib.Path('love_and_piece/1')  # создаём папку для первого абзаца
p.mkdir(exist_ok=True)
s = [x for x in f]  # записываем файл в список
c = 1  # эта переменная хранит номер абзаца
for i in range(len(s)):
    if i > 0:
        if s[i-1][-2:] == "\n":
            c += 1
            p = pathlib.Path(f'love_and_piece/{str(c)}')  # создаём папку для следующего абзаца
            p.mkdir(exist_ok=True)
            s1 = s[i].split(".")
            k = 0  # номер "предложения" в абзаце
            for j in range(len(s1)):
                k += 1
                e = open(f'love_and_piece/{str(c)}/{str(k)}.txt', 'w')  # создаём файл для следующего абзаца
                e.write(s[i])  # записываем в файл текст следующего абзаца
                e.close()  # закрываем файл
        else:
            s1 = s[i].split(".")
            k = 0  # номер "предложения" в абзаце
            for j in range(len(s1)):
                k += 1
                e = open(f'love_and_piece/{str(c)}/{str(k)}.txt', 'w')  # создаём файл для следующего предложения
                e.write(s[i])  # записываем в файл текст следующего предложения
                e.close()  # закрываем файл
    else:
        p = pathlib.Path(f'love_and_piece/{str(c)}')  # создаём папку для первого абзаца
        p.mkdir(exist_ok=True)
        e = open(f'love_and_piece/{str(c)}/1.txt', 'w')  # создаём файл для первого абзаца
        e.write(s[i])  # записываем в файл текст первого абзаца
        e.close()
f.close()  # закрываем исходный файл
