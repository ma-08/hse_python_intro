import pandas as pd
import json


def main():
    data = pd.read_csv("https://raw.githubusercontent.com/aaaksenova/NLP-Homework/main/movie_metadata.csv")
    stats(data)
    df2json(data)


def stats(data):
    #  считаем процент цветных фильмов, разделив количество цветных на общее количество
    d = data["color"].value_counts()[0] / (data["color"].value_counts()[0] + data["color"].value_counts()[1]) * 100
    print(f"Процент цветных фильмов {d}")
    directors_films = {}  # создаём словарь, в котором собираем режиссёров (ключи) и количество их фильмов
    for director in data['director_name']:
        if director in directors_films:  # если режиссёр уже есть в словаре, увеличиваем кол-во его фильмов
            directors_films[director] += 1
        elif isinstance(director, str):  # записываем в словарь только строки, чтобы избежать NaN
            directors_films[director] = 1
    # сортируем словарь по значениям
    sorted_directors_films = dict(sorted(directors_films.items(), key=lambda item: item[1], reverse=True))
    n = list(sorted_directors_films.keys())[:5]  # делаем срез первых 5 самых частотных режиссёров
    m = list(sorted_directors_films.values())[:5]  # и первых 5 значений кол-ва их фильмов
    print("Топ-5 самых частотных режиссеров и число их фильмов в датасете:")
    for i in range(len(n)):
        print(f"{n[i]} - {m[i]}")
    print(f'Средняя длина фильма {data["duration"].mean()}')
    print(f'Медианная длина фильма {data["duration"].median()}')
    f = len(data[(data["plot_keywords"].str.contains("romance") | data["plot_keywords"].str.contains("love")) &
                 (data["imdb_score"] > 7)])  # определяем фильмы с описанием love или romance с imdb больше 7
    print("Число фильмов, в ключевых словах которых есть слово love или слово romance"
          f" и оценка которых на imdb больше 7, составляет {f}")
    d2 = {}  # создаём словарь для записи жанров и их количества
    for x in data["genres"]:
        s = x.split("|")  # разбиваем перечень жанров фильма на отдельные жанры
        for y in s:
            if y not in d2:  # если жанра нет в словаре, добавляем
                d2[y] = 1
            else:  # если жанр есть в словаре, увеличиваем количество фильмов с ним
                d2[y] += 1
    print("Топ-5 самых частотных жанров")
    # выводим первые пять жанров из списка отсортированных по значениям ключей словаря
    print(*list(dict(sorted(d2.items(), key=lambda item: item[1], reverse=True)).keys())[:5])


def df2json(data):
    s = []  # список для словарей-строк
    e = 0  # в этой переменной будем хранить номер строки
    for line in data.iloc():  # проходим по каждой строке датасета
        k = 0  # в этой переменной будем хранить номер столбца
        d4 = {}  # словарь столбец-значение
        for x in data.iloc[e]:  # для каждой строки
            d4[list(data.columns)[k]] = str(x)  # записываем в словарь название столбца и его значение в данной строке
            k += 1  # увеличиваем номер столбца
        s.append(d4)  # записываем полученный из строки словарь в массив
        e += 1  # увеличиваем номер столбца
    with open('movie_metadata.json', 'w', encoding='utf-8') as f:  # записываем массив в json
        json.dump(s, f, indent=True, ensure_ascii=False)


if __name__ == "__main__":
    main()
