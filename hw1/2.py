name = input("Введите Ваше имя >>")  # запрос ввода имени
name = name.strip()                  # удаление висячих пробелов
name = name.capitalize()             # изменение первой буквы на заглавную
print(f"Хорошего дня, {name}")       # вывод приветствия
