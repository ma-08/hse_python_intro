import pandas as pd
from collections import Counter

data = pd.read_csv("final_review_dataset.csv")
print(f"Топ-3 банка по количеству отзывов {Counter(data['bank']).most_common(3)}")
rating = [x for x in data['rating_value']]
print(f"{str(Counter(rating).items())[12:]}")
max_len = 0
min_len = 10000000
max_bank = ''
min_bank = ''
for i in range(len(data['review'])):
    if len(data['review'][i]) > max_len:
        max_len = len(data['review'][i])
        max_bank = data.loc[i][0]
    if len(data['review'][i]) < min_len:
        min_len = len(data['review'][i])
        min_bank = data.loc[i][0]
print(max_len)
print(min_len)
print(max_bank)
print(min_bank)
df = data.drop(['bank', 'type', 'user_name', 'review_title', 'review_dttm', 'review_views', 'review_comments',
                'Вежливые сотрудники', 'Доступность и поддержка', 'Прозрачные условия',
                'Удобство приложения, сайта', 'review_len'], axis=1)
for i in range(len(df)):
    if 1 < df.loc[i][1] < 5:
        df.drop([i], axis=0)
    if df.loc[i][1] == None or df.loc[i][0] == None:
        df.drop([i], axis=0)
df.to_csv('selected_reviews.tsv', sep='\t')
good = [df.loc[x][0] for x in range(len(df)) if df.loc[x][1] == "5.0"]
bad = [df.loc[x][0] for x in range(len(df)) if df.loc[x][1] == "1.0"]