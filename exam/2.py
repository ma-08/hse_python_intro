class BankAccount:
    def __init__(self, account_number, balance, owner_name):
        if isinstance(account_number, int) and isinstance(balance, float) \
                and isinstance(owner_name, str):
            self.account_number = account_number
            self.balance = balance
            self.owner_name = owner_name
        else:
            raise Exception('Неправильный формат ввода. Попробуйте ещё раз')

    def deposit(self, amount):
        if isinstance(amount, float) or isinstance(amount, int):
            self.balance += amount
        else:
            raise Exception('Неправильный формат ввода. Попробуйте ещё раз')

    def withdraw(self, amount):
        if isinstance(amount, float) or isinstance(amount, int):
            r = self.balance - amount
            if r > 0:
                self.balance -= amount
            else:
                print('Недостаточно средств')
        else:
            raise Exception('Неправильный формат ввода. Попробуйте ещё раз')

    def get_balance(self):
        return f"Баланс {self.balance}"

    def get_account_number(self):
        return f"Номер счета {self.account_number}"

    def get_owner_name(self):
        return f"ФИО {self.owner_name}"

    def __str__(self):
        return f"ОАО БАНК РОМАШКОВОЙ ДОЛИНЫ. Данные счета:\n ФИО {self.owner_name}\n Номер счета {self.account_number} "


cr = BankAccount(123, 1245.0, "Крош Александр Семенович")
cr.withdraw(5)
cr.withdraw(10000000)
cr.withdraw(7)
cr.deposit(0.5)
print(cr.get_balance())
